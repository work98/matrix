#pragma once
#include "Vector.h"
#include <math.h>
#include <exception>
#include <algorithm>

using namespace std;

class Matrix : protected Vector<Vector<float>> {
	int cols;
	int rows;
public:
	Matrix(int cols, int rows) : Vector<Vector<float>>(cols) {
		this->cols = cols;
		this->rows = rows;
		for (int i = 0; i < cols; i++) {
			this->val[i] = Vector<float>(rows);
			for (int j = 0; j < rows; j++) {
				this->val[i].val[j] = 0;
			}
		}
	}
	void randinit() {
		for (int i = 0; i < cols; i++) {
			for (int j = 0; j < rows; j++) {
				this->val[i].val[j] = (((float)rand()/(float)RAND_MAX)-0.5)*100.0f;
			}
		}
	}

	float get(int col, int row) {
		return this->val[col].val[row];
	}

	void set(int col, int row, float val) {
		this->val[col].val[row] = val;
	}

	Matrix operator+(Matrix a) {
		if ((a.cols != this->cols) || (a.rows != this->rows)) {
			printf("Matrixes are incompatible");
			return;
		}
		int cc = std::max(a.cols, this->cols);
		int rc = std::max(a.rows, this->rows);

		Matrix buff = Matrix(cc, rc);

		for (int c = 0; c < cc; c++) {
			for (int r = 0; r < rc; r++) {
				float t = 0;
					t += get(c, r);
					t += a.get(c, r);
				buff.set(c,r,t);
			}
		}
		return buff;
	}

	Matrix operator*(Matrix a) {
		if (a.cols != this->rows) {
			printf("Matrixes are incompatible");
			return;
		}

		Matrix buff = Matrix(this->cols, a.rows);
		for (int x = 0; x < this->cols; x++) {
			for (int y = 0; y < a.rows; y++) {
				float c = 0.0f;
				for (int r = 0; r < this->rows; r++) {
					c += get(x, r) * a.get(r, y);
				}
				buff.set(x, y, c);
			}
		}
		return buff;
	}

	void print() {
		printf("Matrix[%d,%d]\n",cols, rows);
		for (int w = 0; w < cols; w++) {
			for (int h = 0; h < rows; h++) {
				printf("%f ", this->val[w].val[h]);
			}
			printf("\n");
		}

	}
};
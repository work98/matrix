#pragma once
#include <stdarg.h>
#include <exception>
#include <stdio.h>
#include <math.h>
using namespace std;
template <typename T>
class Vector {
public:
	int dim;
	T* val;
	Vector(int dim) {
		if (dim==0)
			throw exception("zero dim vector?");
		this->dim = dim;
		this->val = new T[dim];
	}
	Vector() {}

	void normalize() {
		this->val = (*this * (1.0f / this->lenght())).vec;
	}

	T lenght() {
		T sum = 0.0f;
		for (int i = 0; i < this->dim; i++) {
			sum += this->val[i] * this->val[i];
		}
		return pow(sum, 0.5f);
	}
	T distance(Vector a) {
		T sum = 0.0f;
		for (int i = 0; i < this->dim; i++) {
			T diff = this->val[i] - a.val[i];
			sum += diff * diff;
		}
		return pow(sum, 0.5f);
	}
	T dot(Vector a) {
		T sum = 0.0f;
		for (int i = 0; i < this->dim; i++) {
			sum += this->val[i] * a.val[i];
		}
		return sum;
	}

	Vector operator + (Vector a) {
		Vector newvec = Vector(this->dim);
		for (int i = 0; i < this->dim; i++) {
			newvec.val[i] = this->val[i] + a.val[i];
		}
		return newvec;
	}
	void operator += (Vector a) {
		for (int i = 0; i < this->dim; i++) {
			this->val[i] += a.val[i];
		}
	}

	Vector operator - (Vector a) {
		Vector newvec = Vector(this->dim);
		for (int i = 0; i < this->dim; i++) {
			newvec.val[i] = this->val[i] - a.val[i];
		}
		return newvec;
	}
	void operator -= (Vector a) {
		for (int i = 0; i < this->dim; i++) {
			this->val[i] -= a.val[i];
		}
	}

	Vector operator * (T a) {
		Vector newvec = Vector(this->dim);
		for (int i = 0; i < this->dim; i++) {
			newvec.val[i] = this->val[i] * a;
		}
		return newvec;
	}
	void operator *= (T a) {
		for (int i = 0; i < this->dim; i++) {
			this->val[i] *= a;
		}
	}

	bool operator == (Vector a) {
		for (int i = 0; i < this->dim; i++) {
			if (a.val[i] != this->val[i]) return false;
		}
		return true;
	}
	bool operator != (Vector a) {
		return !(this == a);
	}

	void print() {
		printf("{ ");
		for (int i = 0; i < this->dim; i++) {
			printf("%lf ", this->val[i]);
		}
		printf("}\n");
	}


};